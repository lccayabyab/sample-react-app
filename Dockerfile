FROM node:18-alpine3.18 AS build

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn 

copy . ./
RUN yarn build


FROM nginx:1.25.0-alpine
COPY --from=build app/nginx.conf /etc/nginx/nginx.conf
COPY --from=build app/dist /usr/share/nginx/html
EXPOSE 3000 80
CMD ["nginx", "-g", "daemon off;"]